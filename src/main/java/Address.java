import javax.xml.bind.annotation.XmlElement;

public class Address {
    String street;
    String streetNumber;
    String city;

    public String getStreet() {
        return street;
    }

    @XmlElement(name = "street")
    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    @XmlElement(name = "city")
    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetNumber() {
        return streetNumber;
    }
    @XmlElement(name = "streetnumber")
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }
}
