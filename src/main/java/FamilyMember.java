import javax.xml.bind.annotation.XmlElement;

public class FamilyMember {

    String name;
    String birthYear;
    String mobilePhone;
    String homePhone;

    Address address = new Address();

    public String getName() {
        return name;
    }
    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }
    public String getBirthYear() {
        return birthYear;
    }
    @XmlElement(name = "born")
    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    @XmlElement(name = "mobile")
    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getHomePhone() {
        return homePhone;
    }
    @XmlElement(name = "homenumber")
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public Address getAddress() {
        return address;
    }
    @XmlElement(name = "address")
    public void setAddress(Address address) {
        this.address = address;
    }
}