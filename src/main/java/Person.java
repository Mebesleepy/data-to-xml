import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class Person {
    String firstName;
    String lastName;
    String mobilePhone;
    String homePhone;
    List<FamilyMember> family = new ArrayList<>();
    Address address = new Address();

    public String getFirstName() {
        return firstName;
    }
    @XmlElement(name = "firstname")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return mobilePhone;
    }
    @XmlElement(name = "mobile")
    public void setPhone(String phone) {
        this.mobilePhone = phone;
    }

    public String getWorkPhone() {
        return homePhone;
    }
    @XmlElement(name = "workphone")
    public void setWorkPhone(String workPhone) {
        this.homePhone = workPhone;
    }

    @XmlElement(name = "lastname")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLastname() {
        return lastName;
    }

    public List<FamilyMember> getFamily() {
        return family;
    }


    public Address getAddress() {
        return address;
    }
    @XmlElement(name = "")
    public void setAddress(Address address) {
        this.address = address;
    }

    @XmlElement(name = "family")
    public void setFamily(List<FamilyMember> family) {
        this.family = family;
    }

}
