import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.util.Scanner;

public class Main {
    public static PeopleClass peopleClass = new PeopleClass();
    private static boolean addingPerson = false;
    private static boolean addingFamily = false;
    private static String path = "src/main/resources/data.txt";
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            boolean skip = getFileOption();

            if (!skip) {
               path = getUserDefinedPath();
            }

            File fileToRawData = new File(path).getAbsoluteFile();

            BufferedReader br = new BufferedReader(new FileReader(fileToRawData));
            String dataLine;
            while ((dataLine = br.readLine()) != null) {
                sortByFirstLetter(dataLine);
            }

        } catch (IOException ex) {
            System.out.println("Something went wrong with finding or reading the file, please enter a valid path with a" +
                    " file and/or check file permissions");
            System.exit(-1);
        }

        try {
            JAXBContext context = JAXBContext.newInstance(PeopleClass.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter sw = new StringWriter();
            marshaller.marshal(peopleClass, sw);

            FileWriter myWriter = new FileWriter("./People.xml");
            myWriter.write(sw.toString());
            myWriter.close();
            System.out.println("XML file written");

        } catch (JAXBException ex) {
            ex.printStackTrace();
        } catch (IOException e) {
            System.out.println("Could not write to file, please contact your software provider");
            System.exit(-1);
        }
    }

    private static String getUserDefinedPath() {
        System.out.println("What is the file path to the unprocessed file? ex. home/user/code-dir/data.txt");
        path = scanner.nextLine();
        return path;
    }

    private static boolean getFileOption() {
        String selected = "";

        while (true) {
            System.out.println("Do you want to use your own file or the provided test example?");
            System.out.println("1. Own file");
            System.out.println("2. Test example");

            selected = scanner.nextLine();

            if (selected.equals("1")) {
                return false;
            } else if (selected.equals("2")) {
                return true;
            } else {
                System.out.println("That was not a alternative");
            }
        }
    }

    private static void sortByFirstLetter(String line) {

        String[] splitLine = line.split("\\|");

        switch (splitLine[0]) {
            case ("P"):
                addingPerson = true;
                addingFamily = false;

                Person newPerson = new Person();

                newPerson.setFirstName(splitLine[1]);
                newPerson.setLastName(splitLine[2]);

                peopleClass.people.add(newPerson);
                break;

            case ("T"):
                Person personTelephoneUpdate = peopleClass.people.get(peopleClass.people.size() - 1);

                if (addingPerson) {

                    personTelephoneUpdate.setPhone(splitLine[1]);
                    personTelephoneUpdate.setWorkPhone(splitLine[2]);

                } else if (addingFamily) {
                    FamilyMember familyMemberToUpdate = personTelephoneUpdate.getFamily().get(personTelephoneUpdate.family.size() - 1);

                    familyMemberToUpdate.mobilePhone = splitLine[1];
                    familyMemberToUpdate.homePhone = splitLine[2];
                }
                break;

            case ("A"):

                Person personToUpdate = peopleClass.people.get(peopleClass.people.size() - 1);

                if (addingPerson) {
                    personToUpdate.address.street = splitLine[1];
                    personToUpdate.address.city = splitLine[2];

                    if (splitLine.length > 3) {
                        personToUpdate.address.streetNumber = splitLine[3];
                    }
                } else if (addingFamily) {
                    FamilyMember familyMemberToUpdate = personToUpdate.getFamily().get(personToUpdate.family.size() - 1);

                    familyMemberToUpdate.address.street = splitLine[1];
                    familyMemberToUpdate.address.city = splitLine[2];
                    if (splitLine.length > 3) {
                        familyMemberToUpdate.address.streetNumber = splitLine[3];
                    }
                }
                break;

            case ("F"):

                addingPerson = false;
                addingFamily = true;

                FamilyMember newFamilyMember = new FamilyMember();

                newFamilyMember.name = splitLine[1];
                newFamilyMember.birthYear = splitLine[2];

                peopleClass.people.get(peopleClass.people.size() - 1).family.add(newFamilyMember);
                break;

            default:
                System.out.println("Unknown data provided, please check the data for unknown tags or/and notify the developer");
                System.exit(-1);
                break;
        }
    }
}
